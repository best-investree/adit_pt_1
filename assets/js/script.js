// FOR ANIMATE WHEN CLICK MENU
$(document).ready(function(){
  $("a").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      $("#wrapper").addClass('toggled');
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top - 75
      }, 1500);
    } 
  });
});

// FOR HANDLING BUTTON UP
var scrollToTopBtn = document.querySelector(".scrollup");
var rootElement = document.documentElement;

function handleScroll() {
  var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight
  if ((rootElement.scrollTop / scrollTotal ) > 0.10) {
    scrollToTopBtn.classList.add("showBtn")
  } else {
    scrollToTopBtn.classList.remove("showBtn")
  }
}
document.addEventListener("scroll", handleScroll);

// FOR ACTION BUTTON DETAIL
function detailBEST(){
  alert("BEST aims to create a new generation of 1Investree and create many future leaders along with Investree's Experts in technology for strengthening the tribes and also supporting SMEs and people through a seamless and easy experience. #EveryoneCanGrow with Investree");
}

// FOR ACTION BUTTON REGISTER
function register(){
  alert("contact: career@investree.id");
}