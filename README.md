# BESTIE TASK 1

<img src="assets/image/homepage.png" height="200">

## Description

This is a website to display information about BEST starting from what BEST is, what are the conditions for participating in BEST, and what roles are in BEST

## Requirement

Basically this task is only to fulfill the conditions that have been made such as implementing headers, body content, and footers, implementing css, and implementing javascript.

The following describes the provisions that have been carried out:

| No. | Component | Description |
|-----|--------------|-------------|
|1.|HTML| Neat semantics, there are headers, body content, and footers |
|2.|CSS| Using variables, using custom fonts, writing that is easy to understand|
|3.|Javascript| Slightly implement jquery for interactive scrolling, enable/disable buttons to go up, and provide alerts |

## How To Install

### Clone Repo

The first step you have to clone the github repository to your local with the following command

```bash
git clone https://gitlab.com/best-investree/adit_pt_1.git
```

### Run This Command

After previous steps has been done, just click on file index.html. Enjoy the home page